const backPressed = [];
const returnCode = 'back';

window.addEventListener('keyup', (e) => {
    
    backPressed.push(e.key);
    backPressed.splice(-returnCode.length - 1, backPressed.length - returnCode.length);

    if (backPressed.join('').includes(returnCode)){
        window.history.back();
    }

});