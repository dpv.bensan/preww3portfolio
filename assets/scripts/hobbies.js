const pressed1 = [];
const pressed2 = [];
const pressed3 = [];


const modelkits = 'model-kits';
const pictures = 'pictures';

const books = 'books';


window.addEventListener('keyup', (e) => {
    pressed1.push(e.key);
    pressed1.splice(-modelkits.length - 1, pressed1.length - modelkits.length);

    if (pressed1.join('').includes(modelkits)){
        window.location.assign("./model-kits.html")
    }

});

window.addEventListener('keyup', (e) => {
    
    pressed2.push(e.key);
    pressed2.splice(-pictures.length - 1, pressed2.length - pictures.length);

    if (pressed2.join('').includes(pictures)){
        window.location.assign("./pictures.html")
    }

});

window.addEventListener('keyup', (e) => {
    pressed3.push(e.key);
    pressed3.splice(-books.length - 1, pressed3.length - books.length);

    if (pressed3.join('').includes(books)){
        window.location.assign("./books.html")
    }

});