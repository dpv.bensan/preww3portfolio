const pressed1 = [];
const pressed2 = [];
const pressed3 = [];
const pressed4 = [];

const general = 'tell me about';
const skill = 'skill';
const works = 'works';
const contacts = 'contacts';

window.addEventListener('keyup', (e) => {
    pressed4.push(e.key);
    pressed4.splice(-general.length - 1, pressed4.length - general.length);

    if (pressed4.join('').includes(general)){
        window.location.assign("./general.html")
    }

});


window.addEventListener('keyup', (e) => {

    pressed1.push(e.key);
    pressed1.splice(-skill.length - 1, pressed1.length - skill.length);

    if (pressed1.join('').includes(skill)){
        window.location.assign("./skills.html")
    }

});

window.addEventListener('keyup', (e) => {

    pressed2.push(e.key);
    pressed2.splice(-works.length - 1, pressed2.length - works.length);

    if (pressed2.join('').includes(works)){
        window.location.assign("./works.html")
    }


});

window.addEventListener('keyup', (e) => {

    pressed3.push(e.key);
    pressed3.splice(-contacts.length - 1, pressed3.length - contacts.length);

    if (pressed3.join('').includes(contacts)){
        window.location.assign("./contacts.html")
    }


});